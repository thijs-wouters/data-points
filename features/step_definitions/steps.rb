Given(/^I have no data sets$/) do
  data_set_gateway.clear
  expect(data_set_gateway.count).to eq 0
end

Given(/^I have (\d+) data sets?$/) do |number|
  data_set_gateway.clear
  number.to_i.times do
    data_set_gateway.save(DataSet.new({:name => 'name', :description => 'description'}))
  end

  expect(data_set_gateway.count).to eq number.to_i
end

When(/^I create a data set with$/) do |data_set_to_create|
  CreateDataSet.new(data_set_presenter).execute(:data_set => Hash[data_set_to_create.rows_hash.map { |key, value| [key.to_sym, value] }])
end

Then(/^the data set gateway contains (\d+) datasets?$/) do |number|
  expect(data_set_gateway.count).to eq number.to_i
end

And(/^the saved data set has '(.*)' as '(.*)'/) do |value, field|
  expect(data_set_gateway.get(data_set_presenter.reference).send(field)).to eq value
end

And(/^the saved data set has a reference$/) do
  expect(data_set_gateway.get(data_set_presenter.reference).reference).to_not be_nil
end

And(/^the presented data set has '(.*)' as '(.*)'/) do |value, field|
  expect(data_set_presenter.send(field)).to eq value
end

And(/^the presented data set has a reference$/) do
  expect(data_set_presenter.reference).to_not be_nil
end

Then(/^there are no errors$/) do
  expect(data_set_presenter.errors).to be_empty
end

Then(/^there is (\d+) error$/) do |number_of_errors|
  expect(data_set_presenter).to have(number_of_errors).errors
end

Then(/^the error code is '(.*)'$/) do |value|
  expect(data_set_presenter.error_key).to eq value
end

When(/^I list all the data sets$/) do
  ListDataSets.new(multi_data_sets_presenter).execute({})
end

Then(/^I should receive (\d+) datasets$/) do |number|
  expect(multi_data_sets_presenter).to have(number).data_sets
  number.to_i.times do |index|
    expect(multi_data_sets_presenter.data_sets[index].reference).to_not be_nil
  end
end

