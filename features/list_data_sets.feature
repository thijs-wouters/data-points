Feature: List data sets
  As a user
  In order to know what data I have collected
  I want to list my data sets

  Input:
  Output:
    data_sets => Array
      data_set => Hash
        reference => String
        name => String
        description => String

  Scenario: List 5 data sets
    Given I have 5 data sets
    When I list all the data sets
    Then I should receive 5 datasets