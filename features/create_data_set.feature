Feature: Create data set
  As a user
  In order to group the data points
  I want to create a data set

  Input:
    data_set => Hash
      name => String
      description => String
  Output:
    data_set => Hash
      reference => String
      name => String
      description => String
    errors => Array
      error => Hash
        key => String
        message => String

  Scenario: Enter first data set
    Given I have no data sets
    When I create a data set with
      | name        | name        |
      | description | description |
    Then the data set gateway contains 1 dataset
    And  the presented data set has a reference
    And  the presented data set has 'name' as 'name'
    And  the presented data set has 'description' as 'description'
    And  the saved data set has a reference
    And  the saved data set has 'name' as 'name'
    And  the saved data set has 'description' as 'description'
    And  there are no errors

  Scenario: Enter a second data set
    Given I have 1 data set
    When I create a data set with
      | name        | test    |
      | description | testing |
    Then the data set gateway contains 2 datasets
    And  the presented data set has a reference
    And  the presented data set has 'test' as 'name'
    And  the presented data set has 'testing' as 'description'
    And  the saved data set has a reference
    And  the saved data set has 'test' as 'name'
    And  the saved data set has 'testing' as 'description'
    And  there are no errors

  Scenario: Enter a data set without name
    Given I have no data sets
    When I create a data set with
      | name        |             |
      | description | description |
    Then the data set gateway contains 0 datasets
    And  there is 1 error
    And  the error code is 'missing.name'
