module Mock
  class DataSetGateway
    def initialize
      @data_sets = {}
    end

    def clear
      @data_sets.clear
    end

    def count
      @data_sets.size
    end

    def get(reference)
      @data_sets[reference]
    end

    def all
      DataSetList.new(@data_sets.values)
    end

    def save(data_set)
      data_set_hash = data_set.to_hash
      data_set_hash[:reference] = count + 1
      @data_sets[data_set_hash[:reference]] = DataSet.new(data_set_hash)
    end
  end

  class DataSetPresenter
    def present(result)
      @result = result
    end

    def reference
      @result[:data_set][:reference]
    end

    def name
      @result[:data_set][:name]
    end

    def description
      @result[:data_set][:description]
    end

    def errors
      @result[:errors]
    end

    def error_key
      @result[:errors][0][:key]
    end
  end

  class MultiDataSetsPresenter
    def present(result)
      @result = result
    end

    def data_sets
      @result[:data_sets].map do |data_set|
        presenter = DataSetPresenter.new
        presenter.present(data_set)
        presenter
      end
    end
  end

  MOCK_DATA_SET_GATEWAY = Mock::DataSetGateway.new

  def data_set_gateway
    MOCK_DATA_SET_GATEWAY
  end

  def data_set_presenter
    @data_set_presenter ||= Mock::DataSetPresenter.new
  end

  def multi_data_sets_presenter
    @multi_data_sets_presenter ||= Mock::MultiDataSetsPresenter.new
  end
end

include Mock

World(Mock)