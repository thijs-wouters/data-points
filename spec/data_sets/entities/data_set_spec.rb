require 'rspec'
require 'data_sets/entities/data_set'

describe DataSet do
  subject { DataSet.new(
      :reference => reference,
      :name => name,
      :description => description)
  }

  let(:reference) { 'test reference' }
  let(:name) { 'test name' }
  let(:description) { 'test description' }

  describe '#initialize' do
    [:reference, :name, :description].each do |field|
      it "sets the #{field} from the given hash" do
        expect(subject.send(field)).to eq send(field)
      end
    end
  end

  describe '#to_hash' do
    [:reference, :name, :description].each do |field|
      it "returns the #{field}" do
        expect(subject.to_hash).to include field => send(field)
      end
    end
  end

  context 'when all data valid' do
    let (:name) { 'something' }

    describe '#valid?' do
      it 'returns true' do
        expect(subject).to be_valid
      end
    end

    describe '#errors' do
      it 'has no errors' do
        expect(subject).to have(0).errors
      end
    end
  end

  context 'when empty name' do
    let(:name) { '' }

    describe '#valid?' do
      it 'returns false' do
        expect(subject).to_not be_valid
      end
    end

    describe '#errors' do
      it 'has one error' do
        expect(subject).to have(1).errors
      end

      it 'contains missing name error' do
        expect(subject.errors).to include :key => 'missing.name'
      end
    end
  end

  context 'when no name' do
    let(:name) { nil }

    describe '#valid?' do
      it 'returns false' do
        expect(subject).to_not be_valid
      end
    end

    describe '#errors' do
      it 'has one error' do
        expect(subject).to have(1).errors
      end

      it 'contains missing name error' do
        expect(subject.errors).to include :key => 'missing.name'
      end
    end
  end
end
