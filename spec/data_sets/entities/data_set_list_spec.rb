require 'rspec'
require 'data_sets/entities/data_set_list'

describe DataSetList do
  context 'when empty' do
    subject { DataSetList.new([]) }

    describe '#to_hash' do
      it 'returns empty' do
        expect(subject.to_hash).to be_empty
      end
    end
  end

  context 'when filled' do
    subject { DataSetList.new([data_set]) }
    let(:data_set) { double(:data_set, :to_hash => data_set_hash) }
    let(:data_set_hash) { double(:data_set_hash) }

    describe '#to_hash' do
      it 'returns the hash of each data set' do
        expect(subject.to_hash).to include :data_set => data_set_hash
      end
    end
  end
end