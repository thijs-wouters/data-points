require 'rspec'
require 'data_sets/interactors/list_data_sets'

describe ListDataSets do
  describe '#execute' do
    subject { ListDataSets.new(presenter) }

    let(:presenter) { double(:presenter, :present => nil) }
    let(:data_set_gateway) { double(:data_set_gateway, :all => data_set_list) }
    let(:data_set_list) { double(:data_set_list, :to_hash => data_set_list_hash) }
    let(:data_set_list_hash) { double(:data_set_list_hash) }

    before do
      allow(subject).to receive(:data_set_gateway).and_return(data_set_gateway)
    end

    it 'looks up all the data sets from the gateway' do
      expect(data_set_gateway).to receive(:all)
      subject.execute({})
    end

    it 'presents the hash of the list' do
      expect(presenter).to receive(:present).with(:data_sets => data_set_list_hash)
      subject.execute({})
    end
  end
end