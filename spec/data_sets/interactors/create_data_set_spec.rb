require 'rspec'
require 'data_sets/interactors/create_data_set'
require 'data_sets/entities/data_set'

describe CreateDataSet do
  subject { CreateDataSet.new(presenter) }

  let(:saved_data_set_hash) { double (:saved_data_set_hash) }
  let(:saved_data_set) { double(:saved_data_set, :to_hash => saved_data_set_hash, :errors => errors) }
  let(:data_set_gateway) { double(:data_set_gateway, :save => saved_data_set) }
  let(:new_data_set) { double(:new_data_set, :valid? => true) }
  let(:presenter) { double(:presenter, :present => nil) }
  let(:errors) { double(:errors) }

  before do
    allow(subject).to receive(:data_set_gateway).and_return(data_set_gateway)
    allow(DataSet).to receive(:new).and_return(new_data_set)
  end

  describe '#execute' do
    it 'creates a new dataset from the given data' do
      expect(DataSet).to receive(:new).with({:name => 'test name', :description => 'test description'})
      subject.execute(:data_set => {:name => 'test name', :description => 'test description'})
    end

    it 'presents the error hash' do
      expect(presenter).to receive(:present).with(hash_including(:errors => errors))
      subject.execute(:data_set => {:name => 'test name', :description => 'test description'})
    end

    context 'a valid data set is created' do
      let(:new_data_set) { double(:valid_data_set, :valid? => true) }

      it 'saves the new data set' do
        expect(data_set_gateway).to receive(:save).with(new_data_set)
        subject.execute(:data_set => {:name => 'test name', :description => 'test description'})
      end

      it 'presents the saved data set' do
        expect(presenter).to receive(:present).with(hash_including(:data_set => saved_data_set_hash))
        subject.execute(:data_set => {:name => 'test name', :description => 'test description'})
      end
    end

    context 'an invalid data set is created' do
      let(:new_data_set) { double(:invalid_data_set, :valid? => false, :to_hash => invalid_data_set_hash, :errors => errors) }
      let(:invalid_data_set_hash) { double(:invalid_data_set_hash) }

      it 'does not save the new data set' do
        expect(data_set_gateway).to_not receive(:save).with(new_data_set)
        subject.execute(:data_set => {:name => 'test name', :description => 'test description'})
      end

      it 'presents the invalid data set' do
        expect(presenter).to receive(:present).with(hash_including(:data_set => invalid_data_set_hash))
        subject.execute(:data_set => {:name => 'test name', :description => 'test description'})
      end
    end
  end
end
