# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'datapoints/version'

Gem::Specification.new do |spec|
  spec.name          = 'datapoints'
  spec.version       = Datapoints::VERSION
  spec.authors       = ['Thijs Wouters']
  spec.email         = ['thijs@morewood.be']
  spec.description   = 'This gem holds the business logic for the datapoint app.'
  spec.summary       = spec.description
  spec.homepage      = ''
  spec.license       = 'MIT'

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.3'
  spec.add_development_dependency 'rake'
end
