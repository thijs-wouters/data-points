class DataSetList
  def initialize(data_sets)
    @data_sets = data_sets
  end

  def to_hash
    @data_sets.map { |data_set| {:data_set => data_set.to_hash} }
  end
end
