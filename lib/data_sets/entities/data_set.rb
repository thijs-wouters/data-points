class DataSet
  attr_reader :reference, :name, :description

  def initialize(hash)
    @reference = hash[:reference]
    @name = hash[:name]
    @description = hash[:description]
  end

  def to_hash
    {
        :reference => reference,
        :name => name,
        :description => description,
    }
  end

  def valid?
    errors.empty?
  end

  def errors
    errors = []
    errors << {:key => 'missing.name'} if name.nil? || name.empty?
    errors
  end
end
