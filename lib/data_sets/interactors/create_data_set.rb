class CreateDataSet
  def initialize(presenter)
    @presenter = presenter
  end

  def execute(input)
    data_set = DataSet.new(input[:data_set])
    data_set = data_set_gateway.save(data_set) if data_set.valid?
    @presenter.present(:data_set => data_set.to_hash, :errors => data_set.errors)
  end
end
