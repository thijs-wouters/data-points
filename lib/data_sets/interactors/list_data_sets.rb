class ListDataSets
  def initialize(presenter)
    @presenter = presenter
  end

  def execute(_)
    @presenter.present(:data_sets => data_set_gateway.all.to_hash)
  end
end
